public class Goalie extends Player {
    private int wins;

    public Goalie(String name, int age, int wins) {
        super(name, age);
        this.wins = wins;
    }

    public int getWins() {
        return wins;
    }
}
