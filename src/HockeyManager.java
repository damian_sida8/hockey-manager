import java.util.ArrayList;
import java.util.List;

public class HockeyManager {
    private List<Player> players;

    public HockeyManager() {
        players = new ArrayList<>();
    }

    public void addNewForward(String name, int age, int goals) {

        Forward forward = new Forward(name, age, goals);
        players.add(forward);
    }

    public void addNewDefender(String name, int age, int hits) {
        Defender defender = new Defender(name, age, hits);
        players.add(defender);
    }

    public void addNewGoalie(String name, int age, int wins) {
        Goalie goalie = new Goalie(name, age, wins);
        players.add(goalie);
    }

    public void printNameAndAgeOfTheYoungestPlayer() {
        if (players.isEmpty()) {
            System.out.println("Add some hockey players");
            return;
        }

        Player youngestPlayer = players.get(0);
        for (Player player : players) {
            if (player.getAge() < youngestPlayer.getAge()) {
                youngestPlayer = player;
            }
        }

        System.out.println("The Youngest player is " + youngestPlayer.getName() + " at the age of " + youngestPlayer.getAge());
    }
}